<xsl:transform
	default-mode="webapp"
	expand-text="yes"
	extension-element-prefixes="ixsl"
	version="3.0"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:array="http://www.w3.org/2005/xpath-functions/array"
	xmlns:date="http://exslt.org/dates-and-times"
	xmlns:err="http://www.w3.org/2005/xqt-errors"
	xmlns:f="urn:rh:function"
	xmlns:ixsl="http://saxonica.com/ns/interactiveXSLT"
	xmlns:js="http://saxonica.com/ns/globalJS"
	xmlns:map="http://www.w3.org/2005/xpath-functions/map"
	xmlns:rh="urn:redhouse/1.0"
	xmlns:saxon="http://saxon.sf.net/"
	xmlns:xhtml="http://www.w3.org/1999/xhtml"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xpath-default-namespace="http://www.w3.org/1999/xhtml">

	<!-- The 2048 Game (originated by Gabriele Cirulli) implemented using Saxon-JS XSLT/XPath 3.0 -->
	<!-- Notes: 
			 	 Most state is maintained in the DOM (the high score is in local storage just because). 
			   The game board is processed using a declarative paradigm. 
			   User interactions are handled using XSLT event handlers. 
			   Animation is handled by CSS. -->

	<xsl:param
		as="xs:integer"
		name="size"
		select="(: game board size :)
			(ixsl:query-params()?size,4)[1]" />

	<xsl:param
		as="xs:integer"
		name="timer"
		select="(: animation timer :)
		let $act := 'getComputedStyle(document.documentElement).getPropertyValue(''--timer'');'
		return (ixsl:query-params()?timer,ixsl:eval($act))[1]">
		<!-- use request param or css variable -->
	</xsl:param>

	<xsl:param
		as="xs:integer"
		name="strokelen"
		select="(: touch stroke length :)
		let $act := 'getComputedStyle(document.documentElement).getPropertyValue(''--strokelen'');'
		return (ixsl:query-params()?strokelen,ixsl:eval($act))[1]">
		<!-- use request param or css variable -->
	</xsl:param>
	
	<xsl:param
		as="xs:decimal"
		name="stroketimer"
		select="(: touch stroke timer :)
		let $act := 'getComputedStyle(document.documentElement).getPropertyValue(''--stroketimer'');'
		return (ixsl:query-params()?stroketimer,ixsl:eval($act))[1]">
		<!-- use request param or css variable -->
	</xsl:param>

	<xsl:variable
		as="xs:boolean"
		name="debug"
		select="false()"
		static="yes" />

	<!-- called by index.html -->
	<xsl:template
		name="xsl:initial-template">
		<!-- show the webapp -->
		<xsl:message
			use-when="$debug">Playing 2048 with a {$size}x{$size} board and {$timer}ms
			timer.</xsl:message>

		<!-- initialize -->
		<xsl:apply-templates
			mode="init"
			select="ixsl:page()" />

		<!-- add initial tiles -->
		<xsl:call-template
			name="addNewTile" />
		<xsl:call-template
			name="addNewTile" />

		<!-- control now passes to the XSLT event dispatcher -->
	</xsl:template>

	<!-- handle a keypress (user-pressed or simulated in pointer/touch handlers) -->
	<xsl:template
		match="html"
		mode="ixsl:onkeydown">
		<xsl:message
			use-when="$debug">onkeydown event {ixsl:get(ixsl:event(), 'key')}</xsl:message>
		<!-- always capture reset -->
		<xsl:if
			test="ixsl:get(ixsl:event(), 'key') = ('R', 'r')">
			<xsl:sequence
				select="ixsl:eval('location.reload()')" />
		</xsl:if>
		<!-- reset flag is set when tiles are moving; and at end of game -->
		<xsl:if
			test="not(@rh-reset-mode)">
			<xsl:variable
				name="direction"
				select="
					let $key := ixsl:get(ixsl:event(), 'key') return
						if ($key = ('ArrowLeft', 'Left', 'a', 'A', 'h', 'H')) then 'left'
						else if ($key = ('ArrowRight', 'Right', 'd', 'D', 'l', 'L')) then 'right'
						else if ($key = ('ArrowUp', 'Up', 'w', 'W', 'k', 'K')) then 'up'
						else if ($key = ('ArrowDown', 'Down', 's', 'S', 'j', 'J')) then 'down'
						else ()" />
			<xsl:choose>
				<xsl:when
					test="exists($direction)">
					<!-- while gameboard is being updated, do not allow movement -->
					<xsl:call-template
						name="setResetMode" />
					<xsl:message
						use-when="$debug">Move {$direction}</xsl:message>
					<xsl:variable
						as="map(*)*"
						name="gameboard"
						select="f:getTiles()" />
					<xsl:variable
						as="map(*)*"
						name="updatedGameboard"
						select="$gameboard => f:fold($direction)" />
					<!-- if nothing moved, don't do an update: the move is illegal -->
					<xsl:if
						test="f:tilesMoved($gameboard,$updatedGameboard)">
						<xsl:call-template
							name="updateTiles">
							<xsl:with-param
								as="map(*)*"
								name="tiles"
								select="$updatedGameboard" />
						</xsl:call-template>
						<xsl:choose>
							<xsl:when
								test="not(@rh-youwon) and (some $tile in $updatedGameboard satisfies xs:integer($tile?val) = 2048)">
								<xsl:call-template
									name="youwon" />
							</xsl:when>
							<xsl:otherwise>
								<!-- keep going -->
								<ixsl:schedule-action
									wait="$timer">
									<xsl:call-template
										name="addNewTile" />
								</ixsl:schedule-action>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:if>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
		<!-- This is also called when tiles have moved, after the tile has faded in. 
			   This call is probably made before that occurs; the fade is on a delay timer. -->
		<xsl:call-template
			name="removeResetMode" />
	</xsl:template>

	<xsl:function
		as="xs:boolean"
		name="f:tilesMoved">
		<xsl:param
			as="map(*)*"
			name="boardA" />
		<xsl:param
			as="map(*)*"
			name="boardB" />
		<xsl:sequence
			select="
				let 
					$moved := function($a,$b){ $a?row ne $b?row or $a?col ne $b?col }
				return
					some $a in $boardA satisfies
						let $b := $boardB[?id = $a?id]
						return
							$moved($a,$b)" />
	</xsl:function>

	<xsl:function
		as="map(*)*"
		name="f:getTiles">
		<xsl:message
			use-when="$debug">Getting tiles</xsl:message>
		<xsl:sequence
			select="
			ixsl:page()/id('gametiles')//span
			! map{
			'id'  : xs:string(./@id),
			'val' : xs:string(./@value),
			'row' : xs:integer(./(@position idiv $size)),
			'col' : xs:integer(./(@position mod $size))
			}" />
	</xsl:function>

	<xsl:function
		as="map(*)*"
		name="f:fold">
		<xsl:param
			as="map(*)*"
			name="tiles" />
		<xsl:param
			as="xs:string"
			name="dir" />
		<xsl:message>Sliding {count($tiles)} tiles {$dir}ward</xsl:message>
		<xsl:choose>
			<!-- sequence the tiles appropriately for the direction of movement -->
			<!-- a further refinement would rotate for the right/down cases as well, so that the fold#4 action doesn't need to know about direction -->
			<xsl:when
				test="$dir='left'">
				<xsl:sequence
					select="for $row in (0 to $size - 1) return filter($tiles,function($m){$m?row = $row}) => sort((),function($m){xs:integer($m?col)}) => f:fold($dir,(),0)" />
			</xsl:when>
			<xsl:when
				test="$dir='right'">
				<xsl:sequence
					select="for $row in (0 to $size - 1) return filter($tiles,function($m){$m?row = $row}) => sort((),function($m){xs:integer($m?col)}) => reverse() => f:fold($dir,(),0)" />
			</xsl:when>
			<xsl:when
				test="$dir='up'">
				<xsl:sequence
					select="for $col in (0 to $size - 1) return filter($tiles,function($m){$m?col = $col}) => sort((),function($m){xs:integer($m?row)}) => f:fold($dir,(),0)" />
			</xsl:when>
			<xsl:when
				test="$dir='down'">
				<xsl:sequence
					select="for $col in (0 to $size - 1) return filter($tiles,function($m){$m?col = $col}) => sort((),function($m){xs:integer($m?row)}) => reverse() => f:fold($dir,(),0)" />
			</xsl:when>
		</xsl:choose>
	</xsl:function>

	<xsl:function
		as="map(*)*"
		name="f:fold">
		<xsl:param
			as="map(*)*"
			name="tiles" />
		<xsl:param
			as="xs:string"
			name="dir" />
		<xsl:param
			as="map(*)*"
			name="acc" />
		<xsl:param
			as="xs:integer"
			name="offset" />
		<xsl:message use-when="$debug" xml:space="preserve">fold {$dir} in: {count($tiles) } out:{count($acc)} ptr: {$offset} {for-each($tiles,function($m){$m?id||'@'||$m?row||','||$m?col})} XXX {for-each($acc,function($m){$m?id||'@'||$m?row||','||$m?col})} </xsl:message>
		<xsl:sequence
			select="
			if (count($tiles) eq 0) 
			then
				$acc 
			else
				let 
					$this := head($tiles),  
					(: fetch the tile that resulted from the last fold (could also pass as param, might be faster) :)
					$prev := if (count($acc) != 0) then head(reverse($acc)) else map{'val':0}, 			
					(: true if the previous tile did not result from merge and previous value matches the current tile :)
					$merge := ( $prev?merge = '' ) and ( xs:integer($this?val) eq xs:integer($prev?val) ),
					(: if tiles match, the current tile will move onto the previous tile :)
					$nextOffset  := if ($merge) then $prev?offset else $offset,
					(: update the tile attributes :)
					(: row/col would be simplified if fold#2 did appropriate combo of rotate/reverse/unrotate/unreverse :)
					$tile := map {
						'id'  	: xs:string($this?id),
						'merge' 	: if ($merge) then $prev?id else '', 
						'val' 	: if ($merge) then xs:integer($this?val) * 2 else xs:integer($this?val),
						'offset' 	: $nextOffset, 
						'row' 	:	xs:integer(if ($dir = 'up') then $nextOffset
							else if ($dir = 'down') then $size - $nextOffset - 1
								else $this?row),
						'col' 	: xs:integer(if ($dir = 'left') then $nextOffset
							else if ($dir = 'right') then $size - $nextOffset - 1
								else $this?col)
					}
				return
					(: continue folding :)
					f:fold(tail($tiles),$dir,($acc,$tile),if ($merge) then $offset else $offset + 1)" />
	</xsl:function>

	<xsl:template
		name="updateTiles">
		<xsl:param
			as="map(*)*"
			name="tiles" />
		<!-- the tiles are in the correct order for animation; they were sorted in the fold#2 function -->
		<xsl:message
			use-when="$debug">DOM Update: {count($tiles)} tiles</xsl:message>
		<!-- remove merged tiles first -->
		<xsl:for-each
			select="$tiles">
			<xsl:variable
				name="tile"
				select="current()" />
			<xsl:if
				test="$tile?merge != ''">
				<xsl:call-template
					name="fadeOutTile">
					<xsl:with-param
						name="tileID"
						select="$tile?merge" />
				</xsl:call-template>
				<xsl:call-template
					name="updateScore">
					<xsl:with-param
						name="val"
						select="$tile?val" />
				</xsl:call-template>
			</xsl:if>
			<!-- move the remaining tiles. CSS handles the animation -->
			<xsl:for-each
				select="ixsl:page()/id($tile?id)">
				<ixsl:set-attribute
					name="value"
					select="xs:string($tile?val)" />
				<ixsl:set-attribute
					name="position"
					select="xs:string($tile?row * $size + $tile?col)" />
				<ixsl:set-attribute
					name="row"
					select="xs:string($tile?row)" />
				<ixsl:set-attribute
					name="col"
					select="xs:string($tile?col)" />
			</xsl:for-each>
		</xsl:for-each>
	</xsl:template>

	<xsl:template
		name="updateScore">
		<xsl:param
			as="xs:integer"
			name="val" />
		<xsl:variable
			name="newscore"
			select="xs:integer((ixsl:page()/id('currentscore'),0)[1] + $val)" />
		<xsl:for-each
			select="ixsl:page()/id('currentscore')">
			<xsl:result-document
				href="?."
				method="ixsl:replace-content">
				<xsl:sequence
					select="$newscore" />
			</xsl:result-document>
		</xsl:for-each>
		<xsl:if
			test="$newscore gt xs:integer(ixsl:page()/id('bestscore'))">
			<xsl:for-each
				select="ixsl:page()/id('bestscore')">
				<xsl:result-document
					href="?."
					method="ixsl:replace-content">
					<xsl:sequence
						select="rh:setBestScore($newscore)" />
				</xsl:result-document>
			</xsl:for-each>
		</xsl:if>
	</xsl:template>

	<xsl:template
		name="youwon">
		<xsl:message>2048 -- YOU WON!</xsl:message>
		<xsl:call-template
			name="setResetMode" />
		<xsl:for-each
			select="ixsl:page()/html">
			<ixsl:set-attribute
				name="rh-youwon"
				select="(: flag to ignore future 2048 tiles :) ''" />
		</xsl:for-each>
		<xsl:for-each
			select="ixsl:page()/id('whiteout')">
			<ixsl:set-style
				name="display"
				select="'block'" />
		</xsl:for-each>
		<xsl:for-each
			select="ixsl:page()/id('youwon')">
			<ixsl:set-style
				name="display"
				select="'block'" />
		</xsl:for-each>
	</xsl:template>

	<xsl:template
		match="button[@id='playpast2048']"
		mode="ixsl:onclick">
		<xsl:message>Playing through...</xsl:message>
		<xsl:for-each
			select="ixsl:page()/id('whiteout')">
			<ixsl:set-style
				name="display"
				select="'none'" />
		</xsl:for-each>
		<xsl:for-each
			select="ixsl:page()/id('youwon')">
			<ixsl:set-style
				name="display"
				select="'none'" />
		</xsl:for-each>
	</xsl:template>

	<xsl:template
		match="button[@id='reset']"
		mode="ixsl:onclick">
		<xsl:message>Resetting gameboard</xsl:message>
		<xsl:sequence
			select="f:fireKey(.,'r')" />
	</xsl:template>

	<xsl:function
		name="f:fireKey">
		<xsl:param
			as="element()"
			name="elem" />
		<xsl:param
			as="xs:string"
			name="keyname" />
		<xsl:message
			use-when="$debug">Firing Key {$keyname}</xsl:message>
		<xsl:variable
			as="xs:integer?"
			name="keycode"
			select="map:get(map {'Left':37,'Up':38,'Right':39,'Down':40,'r':82},$keyname)" />
		<xsl:if
			test="not(empty($keycode))">
			<xsl:message
				use-when="$debug">Keycode: {$keycode}</xsl:message>
			<xsl:variable
				name="evObj"
				select="ixsl:call(ixsl:page(),'createEvent',['Events'])" />
			<xsl:sequence
				select="
						ixsl:call($evObj,'initEvent',['keydown',1,1])" />
			<ixsl:set-property
				name="which"
				object="$evObj"
				select="$keyname" />
			<ixsl:set-property
				name="keycode"
				object="$evObj"
				select="$keycode" />
			<ixsl:set-property
				name="key"
				object="$evObj"
				select="$keyname" />
			<xsl:sequence
				select="ixsl:call($elem,'dispatchEvent',[$evObj])" />
		</xsl:if>
	</xsl:function>

	<!-- Adding Tiles to the Board -->

	<xsl:variable
		name="positionRange"
		select="(: position = row * size + col; updated with each tile move :)
			for $position in 0 to ($size * $size - 1) return $position" />

	<xsl:function
		name="f:freePositions">
		<xsl:sequence
			select="let $usedPositions := (ixsl:page()/id('gametiles')/span[@value]/@position) return distinct-values($positionRange[not(. = $usedPositions )])" />
	</xsl:function>

	<xsl:function
		name="f:randomFreePosition">
		<xsl:variable
			name="freePositions"
			select="f:freePositions()" />
		<xsl:sequence
			select="let $free := f:freePositions() return $free[xs:int(js:Math.floor(js:Math.random() * count($free)) + 1)]" />
	</xsl:function>

	<xsl:template
		name="addNewTile">
		<xsl:variable
			name="position"
			select="f:randomFreePosition()" />
		<xsl:variable
			name="id"
			select="(: a javascript UUID generator :)
				ixsl:call(ixsl:window(),'uuidv4',[])" />
		<xsl:choose>
			<xsl:when
				test="empty($position)">
				<xsl:message xml:space="preserve">No place for a new tile! It's probably game over!</xsl:message>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable
					name="value"
					select="
						if (js:Math.random() >= 0.8)
							then '4'
							else '2'" />
				<xsl:message xml:space="preserve">Placing new {$value} tile @ {$position idiv $size||','||$position mod $size}</xsl:message>
				<xsl:result-document
					href="#gametiles"
					method="ixsl:append-content">
					<span
						class="tile"
						id="{$id}"
						position="{$position}"
						style="opacity:0"
						value="{$value}" />
				</xsl:result-document>
				<ixsl:schedule-action
					wait="xs:integer($timer div 2)">
					<xsl:call-template
						name="fadeInTile">
						<xsl:with-param
							name="tileID"
							select="$id" />
					</xsl:call-template>
				</ixsl:schedule-action>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template
		name="fadeInTile">
		<xsl:param
			name="tileID" />
		<xsl:for-each
			select="ixsl:page()/id($tileID)">
			<ixsl:set-style
				name="opacity"
				select="'1.0'" />
		</xsl:for-each>
	</xsl:template>

	<!-- flag used to prevent movement input -->

	<xsl:template
		name="removeResetMode">
		<xsl:for-each
			select="ixsl:page()/html">
			<ixsl:remove-attribute
				name="rh-reset-mode" />
		</xsl:for-each>
	</xsl:template>

	<xsl:template
		name="setResetMode">
		<xsl:for-each
			select="ixsl:page()/html">
			<ixsl:set-attribute
				name="rh-reset-mode"
				select="(: if set, the only allowable next keypress is 'R'eset :) ''" />
		</xsl:for-each>
	</xsl:template>

	<!-- Removing Tiles from the Board -->

	<xsl:template
		name="fadeOutTile">
		<xsl:param
			name="tileID" />
		<xsl:for-each
			select="ixsl:page()/id($tileID)">
			<ixsl:set-style
				name="opacity"
				select="'0'" />
		</xsl:for-each>
		<ixsl:schedule-action
			wait="$timer">
			<xsl:call-template
				name="removeTile">
				<xsl:with-param
					name="tile"
					select="ixsl:page()/id($tileID)" />
			</xsl:call-template>
		</ixsl:schedule-action>
	</xsl:template>

	<xsl:template
		name="removeTile">
		<xsl:param
			name="tile" />
		<xsl:sequence
			select="ixsl:call($tile,'remove',[])" />
	</xsl:template>

	<!-- initialization -->
	<xsl:template
		match="div[@id='gameboard']"
		mode="init">
		<!-- layout the board background -->
		<xsl:result-document
			href="?."
			method="ixsl:append-content">
			<xsl:for-each
				select="0 to $size - 1">
				<xsl:variable
					as="xs:integer"
					name="row"
					select="current()" />
				<div
					class="row">
					<xsl:for-each
						select="0 to $size - 1">
						<span
							class="cell">
							<small
								xsl:use-when="$debug">{$row||','||current()}</small>
						</span>
					</xsl:for-each>
				</div>
			</xsl:for-each>
		</xsl:result-document>
	</xsl:template>

	<xsl:template
		match="span[@id='currentscore']"
		mode="init">
		<xsl:result-document
			href="?."
			method="ixsl:replace-content">0</xsl:result-document>
	</xsl:template>

	<xsl:template
		match="span[@id='bestscore']"
		mode="init">
		<xsl:result-document
			href="?."
			method="ixsl:replace-content">{xs:integer((rh:getBestScore(),0)[1])}</xsl:result-document>
	</xsl:template>


	<!-- Pointer Event Handling -->
	<!--<xsl:template
		match="div[@id='gameboard']"
		mode="ixsl:ongotpointercapture">
		<xsl:variable
			name="ev"
			select="ixsl:event()" />
		<xsl:message use-when="$debug" xml:space="preserve">ongotpointercapture event: pointer {$ev?pointerId} is captured @ {$ev?clientX},{$ev?clientY}</xsl:message>
		<xsl:sequence
			select="(: discards the result of the call :)
			ixsl:call(.,'setPointerCapture',[$ev?touches?0?identifier])[current-date() lt xs:date('2000-01-01')]" />
	</xsl:template>-->

	<!--<xsl:template
		match="div[@id='gameboard']"
		mode="ixsl:onpointermove">
		<xsl:variable
			name="ev"
			select="ixsl:event()" />
		<xsl:message use-when="$debug" xml:space="preserve">onpointermove event: {$ev?pointerType} pointer {$ev?pointerId} is captured @ {$ev?clientX},{$ev?clientY}</xsl:message>
		<!-\-<xsl:sequence
			select="(: discards the result of the call :)
			ixsl:call(current(),'setPointerCapture',[$ev?pointerId])[current-date() lt xs:date('2000-01-01')]" />-\->
	</xsl:template>-->

	<!--<xsl:template
		match="html"
		mode="ixsl:onlostpointercapture">
		<xsl:variable
			name="ev"
			select="ixsl:event()"
			use-when="$debug" />
		<xsl:message use-when="$debug" xml:space="preserve">onlostpointercapture event: {$ev?identifier} pointer lost</xsl:message>
		<xsl:message use-when="$debug" xml:space="preserve">  Delta: {(@rh-fx,@rh-mx,0)[1] - @rh-cx},{(@rh-fy,@rh-my,0)[1] - @rh-cy} in { xs:double(substring-before(substring-after(xs:string(xs:time(saxon:timestamp()) - xs:time(@rh-ts)),'PT'),'S')) * 1000 }ms</xsl:message>
		<xsl:variable
			name="direction"
			select="(: does the movement move far enough in little enough time to be called a stroke? :)
			let 
			$deltaT := xs:double(substring-before(substring-after(xs:string(xs:time(saxon:timestamp()) - xs:time(@rh-ts)),'PT'),'S')) * 1000,
			$deltaX := xs:integer((@rh-fx,@rh-mx,0)[1]) - xs:integer(current()/@rh-cx),
			$deltaY := xs:integer((@rh-fx,@rh-mx,0)[1]) - xs:integer(current()/@rh-cy)
			return
			if (xs:integer($deltaT) lt xs:integer($timer)) then ''
			else if ((-($deltaX) gt $strokelen) and (abs($deltaY) lt $strokelen)) then 'Left'
			else if (($deltaX gt $strokelen) and (abs($deltaY) lt $strokelen)) then 'Right'
			else if ((-($deltaY) gt $strokelen) and (abs($deltaX) lt $strokelen)) then 'Up'
			else if (($deltaY gt $strokelen) and (abs($deltaX) lt $strokelen)) then 'Down'
			else ''" />
		<xsl:message
			use-when="$debug">Swiped {$direction}</xsl:message>
		<!-\- trigger a keydown event; it will be handled by the "ixsl:onkeydown" mode -\->
		<xsl:sequence
			select="(if ($direction!='') then f:fireKey(current(),$direction) else ())" />
	</xsl:template>-->

	<!-- mouse/touch down -->
	<xsl:template
		match="div[@id='gameboard']"
		mode="ixsl:onpointerdown">
		<xsl:variable
			name="ev"
			select="ixsl:event()" />
		<xsl:message use-when="$debug" xml:space="preserve">onpointerdown event {$ev?pointerId} @ {$ev?clientX},{$ev?clientY}</xsl:message>
		<!--<xsl:sequence
			select="(: discards the result of the call :)
			ixsl:call(current(),'setPointerCapture',[$ev?pointerId])[current-date() lt xs:date('2000-01-01')]" />-->
		<ixsl:set-attribute
			name="data-rh-id"
			select="xs:string($ev?pointerId)" />
		<ixsl:set-attribute
			name="data-rh-st"
			select="xs:string(xs:time(saxon:timestamp()))" />
		<ixsl:set-attribute
			name="data-rh-sx"
			select="xs:string($ev?clientX)" />
		<ixsl:set-attribute
			name="data-rh-sy"
			select="xs:string($ev?clientY)" />
	</xsl:template>

	<!--<xsl:template
		match="div[@id='gameboard']"
		mode="ixsl:ontouchstart">
		<xsl:variable
			name="ev"
			select="ixsl:event()" />
		<!-\-<xsl:sequence
			select="(: default action should not be taken as it normally would be. See https://developer.mozilla.org/en-US/docs/Web/API/Event/preventDefault :)
			ixsl:call($ev,'preventDefault',[])" />-\->
		<xsl:message use-when="$debug" xml:space="preserve">ontouchstart event {$ev?touches?0?identifier} @ {$ev?touches?0?screenX},{$ev?touches?0?screenY}</xsl:message>
		<xsl:sequence
			select="(: discards the result of the call :)
			ixsl:call(current(),'setPointerCapture',[$ev?touches?0?identifier])[current-date() lt xs:date('2000-01-01')]" />
		<ixsl:set-attribute
			name="data-rh-id"
			select="xs:string($ev?touches?0?identifier)" />
		<ixsl:set-attribute
			name="data-rh-st"
			select="xs:string(xs:time(saxon:timestamp()))" />
		<ixsl:set-attribute
			name="data-rh-sx"
			select="xs:string($ev?touches?0?screenX)" />
		<ixsl:set-attribute
			name="data-rh-sy"
			select="xs:string($ev?touches?0?screenY)" />
	</xsl:template>-->

	<!--<xsl:template
		match="html"
		mode="ixsl:mousedown">
		<xsl:variable
			name="ev"
			select="ixsl:event()" />
		<xsl:message use-when="$debug" xml:space="preserve">mousedown event {$ev?pointerId} @ {$ev?clientX},{$ev?clientY}</xsl:message>
		<xsl:sequence
			select="ixsl:call(current(),'setPointerCapture',[$ev?pointerId])[current-date() lt xs:date('2000-01-01')]" />
		<ixsl:set-attribute
			name="rh-identifier"
			select="xs:string($ev?pointerId)" />
		<ixsl:set-attribute
			name="rh-ts"
			select="xs:string(xs:time(saxon:timestamp()))" />
		<ixsl:set-attribute
			name="rh-cx"
			select="xs:string($ev?clientX)" />
		<ixsl:set-attribute
			name="rh-cy"
			select="xs:string($ev?clientY)" />
	</xsl:template>-->

	<!-- touch move -->
	<!--<xsl:template
		match="html"
		mode="ixsl:onpointermove">
		<xsl:variable
			name="ev"
			select="ixsl:event()" />
		<xsl:message use-when="$debug" xml:space="preserve">onpointermove event {$ev?pointerId} @ {$ev?clientX},{$ev?clientY}</xsl:message>
		<xsl:if
			test="@rh-identifier = xs:string($ev?pointerId)">
			<xsl:sequence
				select="$ev?preventDefault()[current-date() lt xs:date('2000-01-01')]" />
			<ixsl:set-attribute
				name="rh-ex"
				select="xs:string($ev?clientX)" />
			<ixsl:set-attribute
				name="rh-ey"
				select="xs:string($ev?clientY)" />
		</xsl:if>
		<xsl:sequence
			select="[current-date() lt xs:date('2000-01-01')]" />
	</xsl:template>-->

	<!--<xsl:template
		match="div[@id='gameboard']"
		mode="ixsl:ontouchmove">
		<xsl:variable
			name="ev"
			select="ixsl:event()" />
		<xsl:message use-when="$debug" xml:space="preserve">ontouchmove event {$ev?touches?0?identifier} @ {$ev?touches?0?screenX},{$ev?touches?0?screenY}</xsl:message>
		<xsl:if
			test="@data-rh-id = xs:string($ev?touches?0?identifier)">
			<xsl:sequence
				select="ixsl:call($ev,'preventDefault',[])[current-date() lt xs:date('2000-01-01')]" />
			<ixsl:set-attribute
				name="data-rh-et"
				select="xs:string(xs:time(saxon:timestamp()))" />
			<ixsl:set-attribute
				name="data-rh-ex"
				select="xs:string($ev?touches?0?screenX)" />
			<ixsl:set-attribute
				name="data-rh-ey"
				select="xs:string($ev?touches?0?screenY)" />
		</xsl:if>
	</xsl:template>-->

	<!-- mouse/touch up -->
	<xsl:template
		match="div[@id='gameboard']"
		mode="ixsl:onpointerup">
		<xsl:variable
			name="ev"
			select="ixsl:event()" />
		<xsl:message use-when="$debug" xml:space="preserve">onpointerup event {$ev?pointerId} @ {$ev?clientX},{$ev?clientY}</xsl:message>
		<xsl:message use-when="$debug" xml:space="preserve">pointer moved: {xs:integer($ev?clientX - xs:integer(@data-rh-sx))},{xs:integer($ev?clientY - xs:integer(@data-rh-sy))} in {seconds-from-duration(xs:time(saxon:timestamp()) - xs:time(@data-rh-st))} / {seconds-from-duration(xs:time(saxon:timestamp()) - xs:time(@data-rh-st)) lt $stroketimer}</xsl:message>
		<xsl:if
			test="(: require that this is be same stroke as we started capturing :)
				@data-rh-id = xs:string($ev?pointerId)">
			<xsl:variable
				name="direction"
				select="(: does the movement move far enough in little enough time to be called a stroke? :)
				let 
				$deltaT := xs:time(saxon:timestamp()) - xs:time(@data-rh-st),
				$deltaX := xs:integer($ev?clientX - xs:integer(@data-rh-sx)),
				$deltaY := xs:integer($ev?clientY - xs:integer(@data-rh-sy))
				return
				if (seconds-from-duration($deltaT) gt $stroketimer) then 'Too Slow'
				else if ((-($deltaX) gt $strokelen) and (abs($deltaY) lt $strokelen)) then 'Left'
				else if (($deltaX gt $strokelen) and (abs($deltaY) lt $strokelen)) then 'Right'
				else if ((-($deltaY) gt $strokelen) and (abs($deltaX) lt $strokelen)) then 'Up'
				else if (($deltaY gt $strokelen) and (abs($deltaX) lt $strokelen)) then 'Down'
				else 'Too Short'" />
			<xsl:message
				use-when="$debug">Swiped {$direction}</xsl:message>
			<!-- trigger a keydown event; it will be handled by the "ixsl:onkeydown" mode -->
			<xsl:sequence
				select="f:fireKey(current(),$direction)" />
			<!-- remove state -->
			<ixsl:remove-attribute
				name="data-rh-id" />
			<ixsl:remove-attribute
				name="data-rh-st" />
			<ixsl:remove-attribute
				name="data-rh-sx" />
			<ixsl:remove-attribute
				name="data-rh-sy" />
		</xsl:if>
	</xsl:template>

	<!--<xsl:template
		match="div[@id='gameboard']"
		mode="ixsl:ontouchend">
		<xsl:variable
			name="ev"
			select="ixsl:event()" />
		<xsl:message use-when="$debug" xml:space="preserve">ontouchend event {$ev?touches?0?identifier} @ {$ev?touches?0?screenX},{$ev?touches?0?screenY}</xsl:message>
		<xsl:if
			test="(: this must be the same stroke that we started capturing :)
			@data-rh-identifier = xs:string($ev?touches?0?identifier)">
			
			<xsl:message use-when="$debug" xml:space="preserve">  Delta: { @data-rh-sx - @data-rh-ex },{ @data-rh-sy - @data-rh-ey } in { @data-rh-et - @data-rh-st }ms</xsl:message>
			<xsl:variable
				name="direction"
				select="(: does the movement move far enough in little enough time to be called a stroke? :)
				let 
				$deltaT := xs:double(substring-before(substring-after(xs:string(xs:time(@data-rh-et) - xs:time(@data-rh-st)),'PT'),'S')) * 1000,
				$deltaX := xs:integer(@data-rh-ex - @data-rh-sx),
				$deltaY := xs:integer(@data-rh-ey - @data-rh-sy)
				return
				if (xs:integer($deltaT) lt xs:integer($timer)) then ''
				else if ((-($deltaX) gt $strokelen) and (abs($deltaY) lt $strokelen)) then 'Left'
				else if (($deltaX gt $strokelen) and (abs($deltaY) lt $strokelen)) then 'Right'
				else if ((-($deltaY) gt $strokelen) and (abs($deltaX) lt $strokelen)) then 'Up'
				else if (($deltaY gt $strokelen) and (abs($deltaX) lt $strokelen)) then 'Down'
				else ''" />
			<xsl:message
				use-when="$debug">Swiped {$direction}</xsl:message>
			<!-\- trigger a keydown event; it will be handled by the "ixsl:onkeydown" mode -\->
			<xsl:sequence
				select="(if ($direction!='') then f:fireKey(current(),$direction) else ())" />
			<ixsl:remove-attribute
				name="data-rh-id" />
			<ixsl:remove-attribute
				name="data-rh-st" />
			<ixsl:remove-attribute
				name="data-rh-sx" />
			<ixsl:remove-attribute
				name="data-rh-sy" />
			<ixsl:remove-attribute
				name="data-rh-et" />
			<ixsl:remove-attribute
				name="data-rh-ex" />
			<ixsl:remove-attribute
				name="data-rh-ey" />
		</xsl:if>
	</xsl:template>-->

	<!-- cancel -->
	<!--<xsl:template
		match="html"
		mode="ixsl:onpointercancel">
		<xsl:variable
			name="ev"
			select="ixsl:event()" />
		<xsl:message use-when="$debug" xml:space="preserve">onpointercancel event {$ev?pointerId} @ {$ev?clientX},{$ev?clientY}</xsl:message>
		<xsl:sequence
			select="(: triggers ixsl:onlostpointercapture; discards the result of the call :)
			ixsl:call(current(),'releasePointerCapture',[$ev?pointerId])[current-date() lt xs:date('2000-01-01')]" />
	</xsl:template>-->

	<xsl:template
		match="html"
		mode="ixsl:touchcancel">
		<xsl:variable
			name="ev"
			select="ixsl:event()" />
		<xsl:message use-when="$debug" xml:space="preserve">ontouchcancel event {$ev?touches?0?identifier} @ {$ev?touches?0?screenX},{$ev?touches?0?screenY}</xsl:message>
		<xsl:sequence
			select="(: triggers ixsl:onlostpointercapture; discards the result of the call :)
			ixsl:call(current(),'releasePointerCapture',[$ev?touches?0?identifier])[current-date() lt xs:date('2000-01-01')]" />
	</xsl:template>

	<!-- and there's also touchforcechange and likely other possible interaction events -->



	<!-- Just for kicks, using Browser Local Storage for maintaining state -->
	<xsl:variable
		name="rh:gameStorage"
		select="ixsl:eval('window.localStorage')" />

	<!--<xsl:function
		name="rh-clearStorage">
		<xsl:try
			select="ixsl:call($rh:gameStorage,'clear',[])">
			<xsl:catch>
				<xsl:message xml:space="preserve">Error code: <xsl:value-of select="$err:code" />, Reason: <xsl:value-of select="$err:description" /> </xsl:message>
			</xsl:catch>
		</xsl:try>
	</xsl:function>-->

	<xsl:function
		as="xs:integer"
		name="rh:setBestScore">
		<xsl:param
			as="xs:integer"
			name="bestscore" />
		<xsl:try>
			<xsl:sequence
				select="(ixsl:call($rh:gameStorage,'setItem',[xs:string('score'),$bestscore]),$bestscore)[1]" />
			<xsl:catch>
				<xsl:message xml:space="preserve">Error code: <xsl:value-of select="$err:code" />, Reason: <xsl:value-of select="$err:description" /> </xsl:message>
			</xsl:catch>
		</xsl:try>
	</xsl:function>

	<xsl:function
		as="xs:integer"
		name="rh:getBestScore">
		<xsl:sequence
			select="(ixsl:call($rh:gameStorage,'getItem',['score']),0)[1]" />
	</xsl:function>

	<!--<xsl:function
		name="rh-removeScore">
		<xsl:try
			select="ixsl:call($rh:gameStorage,'removeItem',['score'])">
			<xsl:catch>
				<xsl:message xml:space="preserve">Error code: <xsl:value-of select="$err:code" />, Reason: <xsl:value-of select="$err:description" /> </xsl:message>
			</xsl:catch>
		</xsl:try>
	</xsl:function>-->

</xsl:transform>
